State-Average Orbital-Optimization VQE
======================================

This is a documentation for SA-OO-VQE project. Together with this documentation it's currently in the early phase of
development.

.. note::
   This project is under active development.

.. toctree::
    :maxdepth: 2

    install
    example1
    example2
    example3
    saoovqe

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
